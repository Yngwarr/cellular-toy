#include <cmath>
#include <SFML/Graphics.hpp>

int main() {
    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");

    float maxRadius = 100.f;
    sf::CircleShape shape(maxRadius);
    shape.setFillColor(sf::Color::Green);

    sf::Clock clk;

    while (window.isOpen()) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        shape.setRadius(maxRadius * cos(clk.getElapsedTime().asSeconds()));

        window.clear();
        window.draw(shape);
        window.display();
    }

    return 0;
}
